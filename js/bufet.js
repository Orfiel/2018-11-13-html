var animals = [
    {
        "name": "setonix brachyurus",
        "species": "kuoka krótkoogonowa",
        "age": "3 years",
        "enclosure": "32",
        "feeding": {
            "time": [
                6.00, 18.20
            ],
            "type": [
                "owoce i iliście", "liście koki"
            ]
        }
    },
    {
        "name": "hydrochoerus hydrochaeris",
        "species": "kapibara wielka",
        "age": "8 years",
        "enclosure": "27",
        "feeding": {
            "time": [
                6.15,  20.37
            ],
            "type": [
                 "glony", "trawa"
            ]
        }
    },
    {
        "name": "panthera tigris tigris",
        "species": "tygrys bengalski",
        "age": "12 years",
        "enclosure": "45",
        "feeding": {
            "time": [
                6.10, 18.46
            ],
            "type": [
                "wołowina", "wieprzowina"
            ]
        }
    },
    {
        "name": "corvus coraxs",
        "species": "kruk zwyczajny",
        "age": "28 years",
        "enclosure": "57",
        "feeding": {
            "time": [
                6.15, 18.00
            ],
            "type": [
                "owady", "ryby"
            ]
        }
    }
];

function insertTable(container) {
    var container = document.getElementById(container);
     article = document.createElement("article");
    container.appendChild(article);
    var animalTable = createTable(animals, "lista zwierząt", "animalTable");
    article.appendChild(animalTable);
}

function createTable(animals, name, id) {
    var table = document.createElement("table");
    var caption = document.createElement("caption");
    caption.innerHTML = name;
    table.appendChild(caption);
    table.classList.add(id);
    table.appendChild(createCaption(animals[0]));

    for (var i = 0; i < animals.length; i++) {
    table.appendChild(implementDatas(animals[i]));
    }

    return table;
}

function createCaption(animal) {
    var tr = document.createElement("tr");
    for (var a in animal){
        var th = document.createElement("th");
        th.innerHTML = a;
        tr.appendChild(th);
    }
    return tr;

}


function createFeedingTable(feeding, name, id) {
    var table = document.createElement("table");
    // var caption = document.createElement("caption");
    // caption.innerHTML = name;
    // table.appendChild(caption);
    table.classList.add(id);
    table.appendChild(createCaption(feeding));

    for (var i = 0; i < 2; i++) {
        var tr = document.createElement("tr");
            var tdTime = document.createElement("td");
            var tdType = document.createElement("td");

                tdTime.innerHTML = feeding.time[i];
                tdType.innerHTML = feeding.type[i];

                tr.appendChild(tdTime);
                tr.appendChild(tdType);

        table.appendChild(tr);
    }
    return table;

}

function implementDatas(animal) {

    var keys = Object.keys(animal);
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/getOwnPropertyNames
    var tr = document.createElement("tr");
    for (var a = 0; a < 4; a++) {

        var td = document.createElement("td");
        td.innerHTML = animal[keys[a]];
        tr.appendChild(td);
    }
    var td1 = document.createElement("td");
    var value = animal[keys[4]];


    var feedingTable = createFeedingTable(value, "pora karmienia", "feedingTable");
    td1.appendChild(feedingTable);

    tr.appendChild(td1);

    return tr;
}
function formulageGnerator(container) {

    var container = "#" + container;
    article = $("<article></article>");
    $(container).append(article);

    fieldset = $("<fieldset></fieldset>");
    article.append(fieldset);

    legent = $("<legend></legend>").text("Formularz");
    article.append(legent);
}

//     // var input = $(":checkbox").wrap("<span></span>").text("Wybierz jedną opcję dostawy");
//     input = $( "form input:checkbox" )
//     article.append(input);
// }

// function f() {
//     var input = $( "form input:checkbox" ).css({
//         background: "yellow",
//         border: "3px red solid"
//     });

